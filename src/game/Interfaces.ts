import { Piece, ArrayPos } from "./Piece";
import { Keyboard } from "./Keyboard";
import { Vector2 } from "ts-vector-math";

export class GamePiece extends Piece {
  public pos: ArrayPos;

  public constructor(cells: number[][], pos: ArrayPos) {
    super(cells);
    this.pos = pos;
  }
}

export interface LocalData {
  showDebug: boolean;
}

export interface Props {
  cols: number;
  rows: number;
  colors: string[];
  stepPeriodMs: number;
}

interface GameState {
  level: number;
  score: number;
  cells: number[][];
  currentPiece?: GamePiece;
  nextPiece: Piece;
  isGameOver: boolean;
  isPaused: boolean;
}

interface RenderState {
  startTime: number;
  timeMs: number;

  lastStepMs: number;
  lastRenderMs: number;
  gridPos: Vector2;
  cellSize: number;
  ctx: CanvasRenderingContext2D;
  canvas: HTMLCanvasElement;
}

interface HelperState {
  localData: LocalData;
  keyboard: Keyboard;
}

export type State = GameState & RenderState & HelperState;
