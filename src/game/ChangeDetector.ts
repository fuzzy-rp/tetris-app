let hashMap = new Map<URL, string>();

function reloadOnSourceChange(callback: () => void) {
  hashMap.forEach(async (hash, url) => {
    var newHash = await getHash(url);
    if (hash !== newHash) {
      callback();
    }
    hashMap.set(url, newHash);
  });
}
async function getHash(url: URL): Promise<string> {
  const hash = await getHeader("HEAD", url.toString(), "etag");
  return hash;
}

function getHeader(
  method: string,
  url: string,
  headerName: string
): Promise<string> {
  return new Promise(function(resolve, reject) {
    let xhr = new XMLHttpRequest();
    xhr.open(method, url);
    xhr.setRequestHeader("Cache-Control", "no-cache");
    xhr.onload = function() {
      if (this.status >= 200 && this.status < 300) {
        resolve(xhr.getResponseHeader(headerName) ?? "");
      } else {
        reject({
          status: this.status,
          statusText: xhr.statusText
        });
      }
    };
    xhr.onerror = function() {
      reject({
        status: this.status,
        statusText: xhr.statusText
      });
    };
    xhr.send();
  });
}

export async function attachChangeDetector(
  urls: URL[],
  callback: () => void
): Promise<void> {
  let count = 0;
  for (const url of urls) {
    try {
      const hash = await getHash(url);
      hashMap.set(url, hash);
    } catch {
    } finally {
      if (++count === urls.length)
        setInterval(() => reloadOnSourceChange(callback), 5 * 1000);
    }
  }
}
