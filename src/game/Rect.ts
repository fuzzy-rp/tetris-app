import { Vector2 } from "ts-vector-math";

export class Rect {
  public min: Vector2;
  public max: Vector2;

  public constructor(min: Vector2, max: Vector2) {
    if (min.x > max.x || min.y > max.y) {
      throw new Error(
        `min should be smaller than max: ${JSON.stringify({ min, max })}`
      );
    }

    this.min = min;
    this.max = max;
  }

  public get width(): number {
    return this.max.x - this.min.x;
  }
  public get height(): number {
    return this.max.y - this.min.y;
  }

  public get center(): Vector2 {
    return this.max.add(this.min, new Vector2()).scale(0.5);
  }
}
