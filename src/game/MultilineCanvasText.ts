import { Vector2 } from "ts-vector-math";
import { Rect } from "./Rect";

export enum LineSplitApproach {
  SplitByCharacters, // slower, but gets an exact split
  SplitByRatio // faster, but may not be exact
}

export interface Options {
  rect: Rect;
  lineHeightRatio: number;
  lineSeparator: string;
  wordSeparator: string;
  splitApproach: LineSplitApproach;
}

function* fitText(
  texts: string[],
  width: number,
  lineSeparator: string,
  wordSeparator: string,
  splitApproach: LineSplitApproach,
  ctx: CanvasRenderingContext2D
): Iterable<string> {
  for (const text of texts) {
    const lines = text.split(lineSeparator);

    for (const line of lines) {
      const lw = ctx.measureText(line).width;
      if (lw < width) {
        yield line;
      } else {
        const splitLines: string[] = [];
        switch (splitApproach) {
          case LineSplitApproach.SplitByCharacters:
            splitByWords(line, wordSeparator, ctx, width, splitLines);
            break;
          case LineSplitApproach.SplitByRatio:
            splitByRatio(lw, line, width, splitLines);
            break;
          default:
            throw new Error(`invalid splitApproach: ${splitApproach}`);
        }

        for (const sl of splitLines) {
          yield sl;
        }
      }
    }
  }
}

function splitByRatio(
  lw: number,
  line: string,
  width: number,
  splitLines: string[]
) {
  const averageCharWidth = lw / line.length;
  const charsToTake = Math.floor(width / averageCharWidth);
  let nextSplit = 0;
  do {
    const sub = line.substr(nextSplit, charsToTake);
    splitLines.push(sub);
    nextSplit += charsToTake;
  } while (nextSplit < line.length);
}

function splitByWords(
  line: string,
  wordSeparator: string,
  ctx: CanvasRenderingContext2D,
  width: number,
  splitLines: string[]
) {
  const chars = line.split(wordSeparator);
  const buffer: string[] = [];
  chars.forEach(c => {
    buffer.push(c);
    const currentLine = buffer.join(wordSeparator);
    const w = ctx.measureText(currentLine).width;
    if (w > width) {
      buffer.pop();
      splitLines.push(buffer.join(wordSeparator));
      buffer.length = 0;
      buffer.push(c);
    }
  });
  if (buffer.length) {
    splitLines.push(buffer.join(wordSeparator));
  }
}

export function drawMultilineText(
  ctx: CanvasRenderingContext2D,
  texts: string[],
  opts?: Options
) {
  if (!opts) {
    const { width, height } = ctx.canvas;
    opts = {
      rect: new Rect(Vector2.zero, new Vector2([width, height])),
      lineHeightRatio: 1.3,
      lineSeparator: "\n",
      wordSeparator: "",
      splitApproach: LineSplitApproach.SplitByRatio
    };
  }

  const {
    rect,
    lineHeightRatio,
    lineSeparator,
    wordSeparator,
    splitApproach
  } = opts;

  const lineHeight = ctx.measureText("M").width;
  const x = rect.min.x;
  let y = rect.min.y;
  const height = rect.height;
  const lines = fitText(
    texts,
    rect.width,
    lineSeparator,
    wordSeparator,
    splitApproach,
    ctx
  );
  for (const line of lines) {
    ctx.fillText(line, x, y);
    y += lineHeight * lineHeightRatio;
    if (y >= height) {
      break;
    }
  }
}
