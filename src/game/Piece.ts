export class ArrayPos {
  public r: number;
  public c: number;

  public constructor(r: number, c: number) {
    this.r = Math.floor(r);
    this.c = Math.floor(c);
  }
  public copy(): ArrayPos {
    return new ArrayPos(this.r, this.c);
  }
  public equals(other: ArrayPos): boolean {
    return this.r === other.r && this.c === other.c;
  }
  public add(offset: ArrayPos): void {
    this.r += offset.r;
    this.c += offset.c;
  }
  public sub(offset: ArrayPos): void {
    this.r -= offset.r;
    this.c -= offset.c;
  }

  public toString(): string {
    return `(${this.r}, ${this.c})`;
  }
}

export class Piece {
  public cells: number[][];

  // public constructor(rows: number, cols: number) {
  //   this.cells = createBoard(rows, cols);
  // }
  public constructor(cells: number[][]) {
    this.cells = cells;
  }

  private rotateCW(): void {
    const { cells } = this;
    const rows = cells.length;
    const cols = cells[0].length;
    const newCells = createBoard(cols, rows);
    for (var r = 0; r < rows; r++) {
      for (var c = 0; c < cols; c++) {
        var cell = cells[r][c];
        newCells[c][rows - 1 - r] = cell;
      }
    }

    this.cells = newCells;
  }
  public isOverlap(boardCells: number[][], pos: ArrayPos): boolean {
    if (pos.r < 0 || pos.c < 0) return true;

    const rows = boardCells.length;
    const cols = boardCells[0].length;

    const { cells } = this;
    const pieceRows = cells.length;
    const pieceCols = cells[0].length;

    for (var r = 0; r < pieceRows; r++) {
      var boardCellR = pos.r + r;
      if (boardCellR >= rows) {
        return true;
      }

      var row = cells[r];
      for (var c = 0; c < pieceCols; c++) {
        var boardCellC = pos.c + c;
        if (boardCellC >= cols) {
          return true;
        }

        var pieceCell = row[c];
        var boardCell = boardCells[boardCellR][boardCellC];

        if (pieceCell > 0 && boardCell > 0) {
          return true;
        }
      }
    }
    return false;
  }

  public rotatePiece(times: number = 1): void {
    times = ((times % 4) + 4) % 4;
    for (var i = 0; i < times; i++) {
      this.rotateCW();
    }
  }

  public randomizeColors(
    colorCount: number,
    oneColorPiece: boolean = true
  ): void {
    const { cells } = this;
    const colorPicker = () => Math.floor(Math.random() * (colorCount - 1) + 1);
    const singleColor = colorPicker();
    const rows = cells.length;
    const cols = cells[0].length;
    for (var r = 0; r < rows; r++) {
      for (var c = 0; c < cols; c++) {
        var cell = cells[r][c];
        if (cell > 0) {
          cells[r][c] = oneColorPiece ? singleColor : colorPicker();
        }
      }
    }
  }

  public forEachCell(callback: (pos: ArrayPos, cell: number) => void) {
    const { cells } = this;
    forEachCell(cells, callback);
  }

  public getColor(): number {
    var color: number = 0;
    this.forEachCell((pos, cell) => (color = cell > 0 ? cell : color));
    return color;
  }
}

const pieces: Piece[] = [
  // J
  new Piece([
    [0, 1],
    [0, 1],
    [1, 1]
  ]),
  // L
  new Piece([
    [1, 0],
    [1, 0],
    [1, 1]
  ]),
  // O
  new Piece([
    [1, 1],
    [1, 1]
  ]),
  // S
  new Piece([
    [0, 1, 1],
    [1, 1, 0]
  ]),
  // Z
  new Piece([
    [1, 1, 0],
    [0, 1, 1]
  ]),
  // T
  new Piece([
    [0, 1, 0],
    [1, 1, 1]
  ]),
  // I
  new Piece([[1, 1, 1, 1]])
];

export function getPiece(): Piece {
  const randomIndex = Math.floor(Math.random() * pieces.length);
  const piece = pieces[randomIndex];
  const cellsCopy = JSON.parse(JSON.stringify(piece.cells));
  return new Piece(cellsCopy);
}

export function createBoard(rows: number, cols: number): number[][] {
  const cells = [];
  for (var r = 0; r < rows; r++) {
    const row = [];
    for (var c = 0; c < cols; c++) {
      row.push(0);
    }
    cells.push(row);
  }
  return cells;
}

export function forEachCell(
  cells: number[][],
  callback: (pos: ArrayPos, cell: number) => void
) {
  const rows = cells.length;
  const cols = cells[0].length;
  for (var r = 0; r < rows; r++) {
    for (var c = 0; c < cols; c++) {
      const cell = cells[r][c];
      const pos = new ArrayPos(r, c);
      callback(pos, cell);
    }
  }
}
