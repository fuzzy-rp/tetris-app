export function writeLocalStorage<T>(key: string, t: T): void {
  const json = JSON.stringify(t);
  localStorage.setItem(key, json);
}
export function readLocalStorage<T>(key: string, factory: () => T): T {
  const json = localStorage.getItem(key);
  if (json) {
    const t: T = JSON.parse(json);
    return t;
  } else {
    const t: T = factory();
    writeLocalStorage(key, t);
    return t;
  }
}

function measureTextNoWrapping(
  ctx: CanvasRenderingContext2D,
  text: string
): { width: number; height: number } {
  return {
    width: ctx.measureText(text).width,
    height: ctx.measureText("M").width
  };
}

export function centerText(
  ctx: CanvasRenderingContext2D,
  text: string
): { dx: number; dy: number } {
  const textSize = measureTextNoWrapping(ctx, text);
  return {
    dx: -textSize.width / 2,
    dy: textSize.height / 2
  };
}
