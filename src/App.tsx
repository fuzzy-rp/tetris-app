import React from "react";
import "./styles/App.css";

import { createRef } from "react";
import { Game } from "./game/Game";
import { attachChangeDetector } from "./game/ChangeDetector";
import FilesToWatch from "./config/filesToWatch.json";

function App() {
  console.error(`Hi from CI_COMMIT_SHA`);

  const gameCanvasRef = createRef<HTMLCanvasElement>();
  const gameCanvas = <canvas ref={gameCanvasRef} width={700} height={600} />;

  window.requestAnimationFrame(() => {
    new Game(gameCanvasRef.current!, {
      stepPeriodMs: 500,
      // colors picked from https://blog.anantshri.info/web-2-0-color-palette/
      colors: [
        "gray",
        "#FF1A00", // red
        "#FF7400", // orange
        "#008C00", // green
        "#4096EE", // light blue
        "#FF0084", // magenta
        "#C79810", // yellow
        "#6BBA70", // cyan
        "#356AA0" // dark blue
      ],
      cols: 10,
      rows: 20
    });
  });

  (async () => {
    const here = window.location.href;
    if (FilesToWatch) {
      FilesToWatch.forEach(file => {
        attachChangeDetector([new URL(file, here)], () => {
          alert(`change to ${file} detected, reloading`);
          window.location.reload();
        });
      });
    }
  })();

  return (
    <div>
      <div>{gameCanvas}</div>
    </div>
  );
}

export default App;
